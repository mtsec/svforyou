# Svforyou Localtunnel change log

### Update ngày 20/08/2020
- điều chỉnh lại network tối ưu về network flow hỗ trợ autossh tốt hơn.
- thêm domain mtdev.design vào hệ thống và người dùng có thể dùng free.
### Fix lỗi 20/08/2020
- lỗi không thể kết nối được ssh do bị giới hạn băng thông

- nguyên nhân do một người dùng nào đó đã share file dung lượng lớn gây quá tải hệ thống

- đã bỏ giới hạn kết nối của bản image do DO cung cấp

```
Enables the UFW firewall to allow only SSH (port 22, rate limited) and 2375/2376 for unencrypted/encrypted communication with the Docker daemon, respectively.
```

- thay đổi IP để không bị hạn chế băng thông cho ssh web chạy nhanh hơn!
